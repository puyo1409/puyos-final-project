// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import { store } from './store'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import firebase from 'firebase'
import VueFire from 'vuefire'


Vue.use(Vuetify, {
  theme: {
    primary: "#222629",
    secondary: "#191414",
    third:"#78c10e",
    accent: "#474B4F",
    error: "#f44336",
    warning: "#fae596",
    info: "#2196f3",
    success: "#4caf50"
  }
})

Vue.use(VueFire)

Vue.config.productionTip = false


const unsubscribe = firebase.auth()
  .onAuthStateChanged((firebaseUser) => {
    new Vue({
      el: '#app',
      router,
      store,
      render: h => h(App),
      created () {
        if (firebaseUser) {
          // User is signed in.
          store.dispatch('autoSignIn', firebaseUser)
          var displayName = firebaseUser.displayName;
          var email = firebaseUser.email;
          var emailVerified = firebaseUser.emailVerified;
          var photoURL = firebaseUser.photoURL;
          var isAnonymous = firebaseUser.isAnonymous;
          var uid = firebaseUser.uid;
          var providerData = firebaseUser.providerData;
        }
      }
    })
    unsubscribe()
  })
