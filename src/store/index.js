import Vue from 'vue'
import firebase from 'firebase'
import {auth, db} from '../firebase'
import Vuex from 'vuex'
import router from '../router'


Vue.use(Vuex);

export const store = new Vuex.Store({
  state: {
    appTitle: 'Puyopuyo',
    user: null,
    error: null,
    loading: false,
    currentChallenge: {
      tier: '',
      name: '',
      questions: [],
      startTime: '',
      chosenAnswer:{}
    }
  },
  mutations: {
    setUser (state, payload) {
      state.user = payload
    },
    setError (state, payload) {
      state.error = payload
    },
    setLoading (state, payload) {
      state.loading = payload
    },
    setCurrentChallenge (state, payload) {
      state.currentChallenge.name = payload.name
      state.currentChallenge.tier = payload.tier
      state.currentChallenge.questions = payload.questions
      // Poon added this
      state.currentChallenge.startTime = payload.startTime
      // state.currentChallenge.chosenAnswer = payload.chosenAnswer

      // console.log(state.currentChallenge.name)
      // console.log(state.currentChallenge.tier)
      // console.log(state.currentChallenge.questions)
    },
    setChosenAnswer(state, payload){ //for resuming.
      state.currentChallenge.chosenAnswer = payload
    }

  },
  actions: {
    createChallenge({commit}, payload) {
      commit('setCurrentChallenge', payload)
      // console.log('setting up challenge')
    },
    createAccount({commit}, payload) {
      commit('setLoading', true)
      // console.log('beforecreate')
      firebase.auth().createUserWithEmailAndPassword(payload.email, payload.password)
        .then(firebaseuser => {
          commit('setUser', {email: payload.email})
          const uid = auth.currentUser.uid
          db.ref('Users/' + uid).set({
            username: payload.username,
            email: payload.email,
            bio: payload.bio,
            trophies: payload.trophies,
            skills: payload.skills,
            pastTests: payload.pastTests
          })
          // console.log('aftercreate')
          router.push('/dashboard')
        })
        .catch(error => {
          commit('setError', error.message)
          commit('setLoading', false)
        })
    },
    autoSignIn ({commit}, payload) {
      commit('setUser', payload)
      router.push('/dashboard')
    },
    userSignIn({commit}, payload) {
      commit('setLoading', true)
      // console.log('beforelogin')
      firebase.auth().signInWithEmailAndPassword(payload.email, payload.password)
        .then(firebaseUser => {
          // console.log('afterlogin')
          commit('setUser', payload)
          commit('setLoading', false)
          commit('setError', null)
          router.push('/dashboard')
        })
        .catch(error => {
          commit('setError', error.message)
          commit('setLoading', false)
        })
    },
    userSignOut ({commit}) {
      firebase.auth().signOut()
      commit('setUser', null)
      router.push('/')
    },
    updateChosenAnswer({commit}, payload) {
      commit('setChosenAnswer', payload)
    }
  },
  getters: {
    isAuthenticated (state) {
      return state.user !== null && state.user !== undefined
    },
    getUser (state) {
      return state.user
    },
    getCurrentChallenge (state) {
      return state.currentChallenge
    },
    getChosenAnswer (state) {
      return state.currentChallenge.chosenAnswer
    }
  }
})
