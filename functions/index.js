import firebase from 'firebase';


const db = firebase.database();



const topicLst = ['algorithm',
  'dataAnalysis',
  'operatingSystem',
  'softwareEngineer'
]

const t1 = {"hard": 1, "medium": 4, "easy": 10};
const t2 = {"hard": 3, "medium": 8, "easy": 10};
const t3 = {"hard": 5, "medium": 13, "easy": 5};
const diff = ["easy", "medium", "hard"]

export async function generateQuestion(topic, tier){

  if (topicLst.includes(topic) && tier > 0){

    const path = "/questions/t"+tier+"/"+topic+"/";
    console.log()
    let i;
    var questions = [];
    if (tier==1){
      for (i=0;i<diff.length;i++){
        questions = await getQuestionFromDifficulty(path, diff[i], t1[diff[i]], questions);
        // console.log("GQ", questions);
      };
      return questions;
    }
    else if (tier==2){
      for (i=0;i<diff.length;i++){
        questions = await getQuestionFromDifficulty(path, diff[i], t2[diff[i]], questions);
      };
      return questions
    }
    else if (tier==3){
      for (i=0;i<diff.length;i++){
        questions = await getQuestionFromDifficulty(path, diff[i], t3[diff[i]], questions);
      };
      return questions;

    }
    else{
      return "No such tier exist";
    }
  }
  else{
    return "No such topic exist";
  }
}

export async function getQuestionFromDifficulty(path, difficulty, qNum, qLst) {
  // console.log(path+difficulty+"/")
  const dbRef = firebase.database().ref(path+difficulty+"/");
  var arr = [];

  const snapshot = await dbRef.once('value');
    // .then(snapshot => {
    const questions = snapshot.val();
    // console.log("GQFD", questions, qLst);
    const count = qNum + qLst.length;
    const qPoolSize = Object.keys(questions).length;
    // console.log("questions", questions );
    // console.log("want this much: ", count);
    while(qLst.length < count){
      var randomnumber = Math.floor(Math.random() * qPoolSize);
      if(qLst.includes(questions[randomnumber])) continue;
      // console.log("goes in: ", questions[randomnumber], randomnumber);
      qLst.push(questions[randomnumber]);
    }
    return qLst;
    // });
}


const s1 = {"hard": 21, "medium": 8, "easy": 5};
const s2 = {"hard": 16, "medium": 9, "easy": 4};
const s3 = {"hard": 10, "medium": 6, "easy": 3};


export function submitTest(questions, answers, tier){
  // console.log("qs", questions);
  // console.log("ans", answers);
  let score =0;
  let total =0;
  let schema;
  if (tier==1){
    schema = s1;
  }
  else if (tier==2){
    schema = s2;
  }
  else if (tier==3){
    schema = s3;
  }
  else{
    return "no such tier exist"
  }

  let i;
  let q;
  let ans;
  let correctAns;
  let isCorrect;
  for (i in questions){
    q = questions[i];
    // console.log("q object: ", q);
    ans = answers[q.question];
    correctAns = q.correct;
    // console.log(correctAns);
    let ai;
    for (ai in ans){
      isCorrect = correctAns.includes(ans[ai]);
    }
    if (isCorrect && correctAns.length == ans.length){
      // console.log("found correct")
      score+=schema[q.difficulty];
    }
    total+=schema[q.difficulty];
    // console.log("total",total)
  }
  let graded = [];
  graded.push(score);
  graded.push(total);
  return graded;
}

function msToTime(duration) {
  var milliseconds = parseInt((duration % 1000) / 100),
    seconds = parseInt((duration / 1000) % 60),
    minutes = parseInt((duration / (1000 * 60)) % 60),
    hours = parseInt((duration / (1000 * 60 * 60)) % 24);

  // hours = (hours < 10) ? "0" + hours : hours;
  // minutes = (minutes < 10) ? "0" + minutes : minutes;
  // seconds = (seconds < 10) ? "0" + seconds : seconds;

  return [hours, minutes, seconds, milliseconds];
}

export function getTimeDifference(start){
  const period = [1,0,0] // give 1:00 hour for every test
  var startTime = new Date(start)
  // console.log(startTime)
  var currentTime = new Date()
  // console.log(currentTime)
  const spentTime = msToTime(currentTime-startTime)
  // console.log(spentTime)
  const hours = (period[0]-spentTime[0]< 0)? 0: period[0]-spentTime[0]
  const minutes = (period[1]-spentTime[1]< 0)? 0: period[1]-spentTime[1]
  const seconds = (period[2]-spentTime[2]< 0)? 0: period[2]-spentTime[2]

  let ret = currentTime-startTime
  // console.log(ret)
  return ret
}
const trophyRule = {
  "tier1Master" : [1, 100],
  "tier2Master" : [2, 100],
  "tier3Master" : [3, 100],
  "grandMaster" : [0, 100]
};



export async function checkTrophy(uid) {
  const userRef = firebase.database().ref("/Users/"+uid+"/");
  const dbRef = firebase.database().ref("/trophy/");
  const userSnapShot = await userRef.once("value");
  const snapshot = await dbRef.once("value");
  const user = userSnapShot.val();
  // ------------- no pastTest ------------
  if (!user.pastTest){
    return [];
  }
  // --------------------------------------
  const pastTest = user.pastTest;
  const scoreSheet = getMaxScore(pastTest);
  let tid;
  let topic;
  let t1Score = 0;
  let t2Score = 0;
  let t3Score = 0;
  for (tid in topicLst){
    topic = topicLst[tid];
    let i;
    for (i=1;i<4;i++){
      if (scoreSheet[topic+i]){
        if (i==1){
          t1Score+=scoreSheet[topic+i]
        }
        else if (i==2){
          t2Score+=scoreSheet[topic+i]
        }
        else if (i==3){
          t3Score+=scoreSheet[topic+i]
        }
      }
    }
  }

  let userTrophy;
  if (!user.trophy){
    userTrophy = [];
  }
  else{
    userTrophy = user.trophy;
  }

  let newTrophy = [];
  const trophy = snapshot.val();
  let t;
  let trophyTier;
  let isEligible;
  for (t in trophy){
    if (!userTrophy.includes(t)){
      trophyTier = trophyRule[t][0];
      if (trophyTier <= 1){
        isEligible = isUserEligible(t, t1Score, userTrophy);
      }
      else if (trophyTier == 2){
        isEligible = isUserEligible(t, t2Score, userTrophy);
      }
      else if (trophyTier == 3){
        isEligible = isUserEligible(t, t3Score, userTrophy);
      }

      if (isEligible){
        newTrophy.push(t);
      }
    }
  }
  // console.log(scoreSheet);
  return newTrophy;
}

const totalScores = {
  "tier1" : 103,
  "tier2" : 160,
  "tier3" : 143
}

export function getMaxScore(pastTest) {
  let score = {};
  let pid;
  let t;
  let topic;
  let tier;
  for (pid in pastTest){
    t = pastTest[pid];
    topic = t.skill;
    tier = t.tier;
    if (!score[topic+tier]){
      score[topic+tier] = t.score;
    }
    else if (t.score > score[topic+tier]){
      score[topic+tier] = t.score;
    }

  }
  return score;
}


export function isUserEligible(trophy, userScore, userTrophy){
  if (trophy == "grandMaster"){
    return userTrophy.includes("tier1Master") &&
      userTrophy.includes("tier2Master") &&
      userTrophy.includes("tier3Master");
  }

  const tier = trophyRule[trophy][0];
  const minPercent = trophyRule[trophy][1];
  const outOf = totalScores["tier"+tier];
  // console.log(tier, userScore);
  // console.log(outOf);
  return ((userScore*100.0)/(outOf*4.0) >= minPercent);
}
