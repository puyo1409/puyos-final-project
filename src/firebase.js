import firebase from 'firebase'

var config = {
  apiKey: "AIzaSyDRNNIr0b8p5_04Da_pLBWTE9TGjv5Cb2o",
  authDomain: "puyo-4858a.firebaseapp.com",
  databaseURL: "https://puyo-4858a.firebaseio.com",
  projectId: "puyo-4858a",
  storageBucket: "puyo-4858a.appspot.com",
  messagingSenderId: "368537432465"
};

firebase.initializeApp(config)

export default firebase
export const db = firebase.database()
export const auth = firebase.auth()
export const storage = firebase.storage()
