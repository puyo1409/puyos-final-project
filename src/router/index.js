import Vue from 'vue'
import Router from 'vue-router'
import firebase from 'firebase'
import HelloWorld from '@/components/Landing/HelloWorld'
import Dashboard from '@/components/Dashboard/Dashboard'
import Challenge from '@/components/Challenge/challenge'
import Test from '@/components/Test/test'
import NotFound from '@/components/Landing/Notfound'
import EditProfile from '@/components/Dashboard/EditProfile'

Vue.use(Router)

const ifAuthenticated = (to, from, next) => {
  if (firebase.auth().currentUser) {
    next()
  } else {
    next('/')
  }
}

const ifNotAuthenticated = (to, from, next) => {
  if (!firebase.auth().currentUser) {
    next()
    return
  }
  next('/')
}


export default new Router({
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld,
      // beforeEnter: ifNotAuthenticated
    },
    {
      path: '/dashboard',
      name: 'Dashboard',
      component: Dashboard,
      beforeEnter: ifAuthenticated
    },
    {
      path: '/editprofile',
      name: 'EditProfile',
      component: EditProfile,
      beforeEnter: ifAuthenticated
    },
    {
      path: '/challenge',
      name: 'Challenge',
      component: Challenge,
      beforeEnter: ifAuthenticated
    },
    {
      path: '/test',
      name: 'Test',
      // props: true,
      component: Test,
      // beforeEnter: ifAuthenticated
    },
    {
      path: '*',
      name: 'NotFound',
      component: NotFound
    }
  ]
})
